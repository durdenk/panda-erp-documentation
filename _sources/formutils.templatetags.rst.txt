formutils.templatetags package
==============================

Submodules
----------

formutils.templatetags.custom\_tags module
------------------------------------------

.. automodule:: formutils.templatetags.custom_tags
    :members:
    :undoc-members:
    :show-inheritance:

formutils.templatetags.widget\_tweaks module
--------------------------------------------

.. automodule:: formutils.templatetags.widget_tweaks
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: formutils.templatetags
    :members:
    :undoc-members:
    :show-inheritance:
