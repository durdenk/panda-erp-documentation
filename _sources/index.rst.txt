.. Panda documentation master file, created by
   sphinx-quickstart on Sun Sep 30 23:19:01 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Panda's documentation!
=================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules/views

External app support
--------------------

.. toctree::
   :maxdepth: 2

   modules/all-about-me

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
