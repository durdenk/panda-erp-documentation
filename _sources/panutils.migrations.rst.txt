panutils.migrations package
===========================

Submodules
----------

panutils.migrations.0001\_initial module
----------------------------------------

.. automodule:: panutils.migrations.0001_initial
    :members:
    :undoc-members:
    :show-inheritance:

panutils.migrations.0002\_auto\_20180828\_2358 module
-----------------------------------------------------

.. automodule:: panutils.migrations.0002_auto_20180828_2358
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: panutils.migrations
    :members:
    :undoc-members:
    :show-inheritance:
